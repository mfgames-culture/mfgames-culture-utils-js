#!/usr/bin/env node
import * as path from "path";
import stripJsonComments = require("strip-json-comments");
import * as walk from "fs-walk";
import * as fs from "fs-extra";
import * as yaml from "js-yaml";
import yargs = require("yargs");

// Set up the main command-line arguments.
var argv = yargs
    .usage("Usage: $0 [-s source] [-o output] [-q]")
    .default("source", "data")
    .default("output", "dist")
    .alias("s", "source")
    .alias("o", "output")
    .nargs("s", 1)
    .nargs("o", 1)
    .boolean("q")
    .default("q", false)
    .alias("q", "quiet")
    .help("h")
    .alias("h", "help")
    .argv;

// Figure out the paths we need to process.
var dataDirectory = path.resolve(argv.source);
var distDirectory = path.resolve(argv.output);

if (!argv.quiet) console.log("source: " + dataDirectory);
if (!argv.quiet) console.log("output: " + distDirectory);

// Completely clear out the old dist directory.
if (fs.existsSync(distDirectory)) {
	fs.removeSync(distDirectory);
}

fs.mkdirSync(distDirectory);

// Walk through the input files in the dirctory and copy them over while
// stripping out the comments. We match the directory structure of the input
// files in the dist files.
walk.dirsSync(dataDirectory, function(dir: string, file: string, stat: any) {
	// Combine them together into the directories.
	var distDir = dir.replace(dataDirectory, distDirectory);
	var distDataDir = path.join(distDir, file);

	// Create the directory if it doesn't exist.
	if (!fs.existsSync(distDataDir)) {
		fs.mkdirSync(distDataDir);
	}
});

// Now that we have the directory structure, copy the files over. At the same
// time, we build up a hash of all the entries to create a combined file.
var combined = {};
var index = {};

walk.filesSync(dataDirectory, function(dir: string, file: string, stat: any) {
	// If the file isn't a JSON, skip it.
	if (!file.match(/\.(json|yaml)$/)) { return; }

	// Combine them together into the relevant filenames. We normalize the name
    // to always output JSON, even if we have a YAML input.
	var distDir = dir.replace(dataDirectory, distDirectory);
	var dataFilename = path.join(dir, file);
	var distFilename = path.join(
        distDir,
        path.basename(file, path.extname(file)) + ".json");

	if (!argv.quiet) {
        console.log("file:", distFilename.replace(distDirectory, "").substr(1));
    }

	// Load the file into memory. If this is a JSON file we strip off comments
    // otherwise if it is a YAML then we get it via YAML which strips off
    // comments.
    var fileData = fs.readFileSync(dataFilename).toString();
    var data;

    switch (path.extname(dataFilename)) {
        case ".json":
    	    var stripData = stripJsonComments(fileData, {whitespace: true});
            data = JSON.parse(stripData);
            break;
        case ".yaml":
            data = yaml.safeLoad(fileData);
            break;
    }

    // Compare and format the data.
    var formattedData = JSON.stringify(data, null, 2);
	var minData = JSON.stringify(data, null, 0);

	// Write out the two versions.
    var baseName = path.basename(distFilename);

	fs.writeFileSync(distFilename, formattedData);
	fs.writeFileSync(distFilename.replace(".json", ".min.json"), minData);

	// Add the full object to the combined list.
	combined[data.id] = data;

	// Create an index-only version.
	index[data.id] = {
		id: data.id,
		type: data.type,
		version: data.version
	};

	if (data.description) { index[data.id].description = data.description; }
});

// Write out the final files.
var combinedFilename = path.join(distDirectory, "combined.json");
var minCombinedFilename = path.join(distDirectory, "combined.min.json");
var indexFilename = path.join(distDirectory, "index.json");
var minIndexFilename = path.join(distDirectory, "index.min.json");

fs.writeFileSync(combinedFilename, JSON.stringify(combined, null, "\t"));
fs.writeFileSync(minCombinedFilename, JSON.stringify(combined, null, 0));
fs.writeFileSync(indexFilename, JSON.stringify(index, null, "\t"));
fs.writeFileSync(minIndexFilename, JSON.stringify(index, null, 0));
